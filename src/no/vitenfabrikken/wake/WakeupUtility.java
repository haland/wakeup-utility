package no.vitenfabrikken.wake;

import no.vitenfabrikken.wake.model.config.ComputerList;
import no.vitenfabrikken.wake.model.wakeup.MACAddress;
import no.vitenfabrikken.wake.model.wakeup.WakeOnLan;
import no.vitenfabrikken.wake.model.wakeup.exceptions.IllegalMACAddressException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;

public class WakeupUtility {
    public static void main(String[] args) {
        HashMap<String, String> argMap = new HashMap<String, String>();
        for (int i = 0; i < args.length-1; i+=2) {
            argMap.put(args[i], args[i+1]);
        }

        String computerListPath = argMap.get("-path") != null ? argMap.get("-path") : ComputerList.DEFAULT;
        System.out.println("Reading MAC adresses from \"" + computerListPath + "\"");

        ComputerList computerList = new ComputerList(computerListPath);

        try {
            int port = argMap.get("-port") != null ? Integer.valueOf(argMap.get("-port")) : WakeOnLan.DEFAULT_PORT;
            String ip = argMap.get("-ip") != null ? argMap.get("-ip") : WakeOnLan.DEFAULT_BROADCAST_ADDRESS;

            InetAddress inetAddress = InetAddress.getByName(ip);

            Iterator<String> i = computerList.iterator();
            while(i.hasNext()) {
                MACAddress macAddress = new MACAddress(i.next());
                WakeOnLan.wakeup(macAddress, inetAddress, port);
                System.out.println("Broadcast magic packet on " + ip + ", using port " + port +
                        ". Packet sent for " + macAddress.toString() + ".");
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IllegalMACAddressException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
