package no.vitenfabrikken.wake.model.wakeup;

import no.vitenfabrikken.wake.model.wakeup.exceptions.IllegalMACAddressException;

import java.io.Serializable;
import java.util.Arrays;
import java.util.StringTokenizer;

public class MACAddress implements Serializable, Cloneable {
    private static final long serialVersionUID = -4066446307050993257L;

    private final byte[] bytes;
    private final static String MAC_ADDRESS_DELIMITER = ":";

    public MACAddress(String macAddress) throws IllegalMACAddressException {
        super();
        this.bytes = parseMACAddress(macAddress);
    }

    protected byte[] parseMACAddress(String macAddress) throws IllegalMACAddressException {
        StringTokenizer tokenizer = new StringTokenizer(macAddress, MAC_ADDRESS_DELIMITER);

        if (tokenizer.countTokens() != 6) {
            throw new IllegalMACAddressException(macAddress + " is not a legal MAC address");
        }

        byte[] macAddressBytes = new byte[6];

        try {
            for (int i = 0; i < 6; i++) {
                String byteToken = tokenizer.nextToken();

                macAddressBytes[i] = (byte) Integer.parseInt(byteToken, 16);
            }
        } catch (NumberFormatException e) {
            throw new IllegalMACAddressException(macAddress + " is not a legal MAC address");
        }

        return macAddressBytes;
    }

    public MACAddress(byte[] macAddress) throws IllegalMACAddressException {
        super();

        if (null == macAddress || macAddress.length != 6)
            throw new IllegalMACAddressException("An ethernet address must be 6 bytes");

        byte[] copyBytes = new byte[macAddress.length];
        System.arraycopy(macAddress, 0, copyBytes, 0, macAddress.length);
        this.bytes = copyBytes;
    }


    public byte[] toBytes() {
        byte[] copy = new byte[this.bytes.length];
        System.arraycopy(this.bytes, 0, copy, 0, this.bytes.length);

        return copy;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (null == obj) {
            return false;
        }

        if (MACAddress.class == obj.getClass()) {
            MACAddress other = (MACAddress) obj;

            return Arrays.equals(this.bytes, other.bytes);
        }

        return false;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < this.bytes.length; i++) {
            byte b = this.bytes[i];

            buffer.append(byteToHexString(b));

            if (i < this.bytes.length - 1) {
                buffer.append(MACAddress.MAC_ADDRESS_DELIMITER);
            }
        }

        return buffer.toString();
    }

    protected String byteToHexString(byte b) {
        return Integer.toString((b & 0xff) + 0x100, 16).substring(1).toUpperCase();
    }

    public int hashCode() {
        int hash = 734;

        for (int i = 0; i < bytes.length; i++) {
            hash += bytes[i];
        }

        return hash;
    }

    public Object clone() {
        try {
            MACAddress clone = new MACAddress(bytes);

            return clone;
        } catch (IllegalMACAddressException e) {
            throw new RuntimeException("Should not happen", e);
        }
    }
}