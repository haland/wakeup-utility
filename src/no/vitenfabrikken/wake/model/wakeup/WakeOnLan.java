package no.vitenfabrikken.wake.model.wakeup;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

public class WakeOnLan {
    public final static int DEFAULT_PORT = 9;
    public final static String DEFAULT_BROADCAST_ADDRESS = "255.255.255.255";

    public final static InetAddress DEFAULT_HOST;

    static {
        InetAddress default_host = null;

        try {
            default_host = InetAddress.getByName(DEFAULT_BROADCAST_ADDRESS);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        DEFAULT_HOST = default_host;
    }

    private WakeOnLan() {
        super();
    }

    public static void wakeup(MACAddress mac) throws IOException {
        WakeOnLan.wakeup(mac, DEFAULT_HOST);
    }

    public static void wakeup(MACAddress mac, InetAddress host) throws IOException {
        WakeOnLan.wakeup(mac, host, DEFAULT_PORT);
    }

    public static void wakeup(MACAddress mac, InetAddress host, int port) throws IOException {
        byte[] wake = createSignal(mac);
        DatagramSocket socket = new DatagramSocket();
        DatagramPacket packet = new DatagramPacket(wake, wake.length, host, port);

        socket.send(packet);
        socket.close();
    }

    protected static byte[] createSignal(MACAddress mac) {
        byte[] macBytes = mac.toBytes();
        byte[] wakeupFrame = new byte[6 + 16 * macBytes.length];

        Arrays.fill(wakeupFrame, 0, 6, (byte) 0xFF);

        for (int i = 6; i < wakeupFrame.length; i += macBytes.length) {
            System.arraycopy(macBytes, 0, wakeupFrame, i, macBytes.length);
        }

        return wakeupFrame;
    }
}