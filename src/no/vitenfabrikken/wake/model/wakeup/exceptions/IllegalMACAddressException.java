package no.vitenfabrikken.wake.model.wakeup.exceptions;

public class IllegalMACAddressException extends Exception {
    private static final long serialVersionUID = -829272444072204615L;

    public IllegalMACAddressException() {
        super();
    }

    public IllegalMACAddressException(String e) {
        super(e);
    }
}