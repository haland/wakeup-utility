package no.vitenfabrikken.wake.model.config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ComputerList {
    public static final String DEFAULT = "config/ComputerList.rec";

    private ArrayList<String> computers;

    public ComputerList(String path) {
        computers = new ArrayList<String>();

        try {
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            while(line != null) {
                char[] lineChars = line.toCharArray();
                char[] macChars = new char[17];

                System.arraycopy(lineChars, 0, macChars, 0, 17);
                String macAddress = new String(macChars);

                computers.add(macAddress.replaceAll("\\-", ":"));
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Iterator<String> iterator() {
        return computers.iterator();
    }
}